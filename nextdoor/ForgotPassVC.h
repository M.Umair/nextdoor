//
//  ForgotPassVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 26/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPassVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btn_mail;
- (IBAction)onClick_btn_Email:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_email;

@end
