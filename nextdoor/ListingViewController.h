//
//  ListingViewController.h
//  nextdoor
//
//  Created by Muhammad Usman on 15/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingViewController : UIViewController <UITableViewDelegate , UITableViewDataSource>

@end
