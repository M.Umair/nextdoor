//
//  RoomDataVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 12/08/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "RoomDataVC.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"

@interface RoomDataVC ()
{
    NSURL *path;
    UIImage *image;
}
@end

@implementation RoomDataVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _sv_roomData.contentSize = CGSizeMake(self.view.frame.size.width, _btn_roomSave.frame.origin.y +_btn_roomSave.frame.size.height+ 10);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_onclick_SaveRoom:(id)sender {
    
    NSDictionary *contactUsDict = [NSDictionary dictionaryWithObjectsAndKeys:_txt_room_Number.text,@"RoomNumber",
                                   _txt_floor_Number.text,@"floorNumber",
                                   nil];
    
    [_FillItDelegate fillData:contactUsDict];
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
//------fields show up when keybord clicks---------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self animateTextField:textField up:NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

//---------end of fields showup code
//- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}
- (IBAction)onClick_btn_selectImage:(id)sender {
    
    _picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
   // _txt_ideaImage.text = @"image";
    //    txt_ideaImage.text = picker.sourceType;
    [self presentViewController:_picker animated:YES completion:NULL];
}

- (IBAction)btn_uploadImage:(id)sender {
    
    NSString *urlString = @"http://mantis.vu.edu.pk";
    NSURL* url = [NSURL URLWithString:urlString];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSData *imageData = UIImageJPEGRepresentation(self.ImageView.image, 0.5);
    
    NSDictionary *prams =@{
    
    @"field_property_image":@{
        @"und":@[
               @{
                   @"fid":@"287",
                   @"uid":@"38",
                   @"filename":@"assests.jpg",
                   @"uri":path,
                   @"type":@"image",
                   
                   }
               ]
        }
    };
        
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"/nextdoor/sites/default/files" parameters:prams constructingBodyWithBlock: ^(id <AFMultipartFormData>formData)
                                    {
                                        //         [formData appendPartWithFileData:imageData name:@"video_file" fileName:@"testvideo.mov" mimeType:@"video/quicktime"];
                                        
                                        [formData appendPartWithFormData:imageData name:@"image"];
                                        
                                    }];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
        float uploadPercentge = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
        float uploadActualPercentage = uploadPercentge *100;
        
        NSLog(@"actual percentage %.2f",uploadActualPercentage);
        
        if (uploadActualPercentage >= 100) {
            NSLog(@"Waitting for response ...");
        }
    }];
    
    [httpClient enqueueHTTPRequestOperation:operation];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //     lblStatus.text = @"Upload Complete";
        NSData *JSONData = [operation.responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableContainers error:nil];
        
        ////Success
        NSLog(@"Idea Created Successfully %@" , jsonObject);
        
    }
                                     failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%@",error);
     }];
    [operation start];
    
    
    self.ImageView.image = image;
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    image = [info valueForKey:UIImagePickerControllerOriginalImage];
    NSLog(@"Image is ...........%@" , image);
    //Or you can get the image url from AssetsLibrary
    path = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSLog(@"%@",path);
    [picker dismissViewControllerAnimated:YES completion:nil];
}



@end
