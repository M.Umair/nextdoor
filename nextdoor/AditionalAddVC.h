//
//  AditionalAddVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AditionalAddVC : UIViewController <UITableViewDataSource , UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tbl_yourview;
@property (strong, nonatomic) IBOutlet UIScrollView *yourlistin_sv;
@property (strong, nonatomic) IBOutlet UITableView *tb_pendingAdview;

@end
