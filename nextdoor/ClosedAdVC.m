//
//  ClosedAdVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "ClosedAdVC.h"
#import "ListingCell.h"

@implementation ClosedAdVC{
    NSArray *tableData;
    NSArray *pics;
    NSArray *time;


}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    tableData = [NSArray arrayWithObjects:@"Usman", @"Umair (MUAKH)", @"Faiza Zahid", @"Amir", @"Maria", @"Aysha Arooj", @"Hafiz Wajahat Hashmi", @"Qasim Ali", @"Saad Sadique", @"Saed Ahmad", @"Ayesha Anjum", @"Faisal Ali", @"Shahla Kiran", @"Muqadas Zahra",nil];
    
    pics = [NSArray arrayWithObjects:@"2.jpg", @"3.jpg", @"4.jpg", @"5.jpg", @"6.jpg", @"7.jpg", @"8.jpg", @"9.jpg", @"10.jpg", @"11.jpg", @"12.jpg", @"13.jpg", @"14.jpg", @"15.jpg",nil];
    return self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellidentifier = @"Cell";
    ListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath] ;
    
    cell.propertnamelbl.text = [tableData objectAtIndex:indexPath.row];
    //cell.propertystatuslbl.text = [propertyStatus objectAtIndex:indexPath.row];
    cell.propImage.image = [UIImage imageNamed:[pics objectAtIndex:indexPath.row]];
    
    
    return cell;
}


@end
