//
//  EditProfileVC.m
//  nextdoor
//
//  Created by Muhammad Umair Ayoub on 18/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "EditProfileVC.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
@interface EditProfileVC ()
{
    NSDictionary *responseDict;
    NSURL *path;
    UIImage *image;
    
}
@end

@implementation EditProfileVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *userState =[prefs stringForKey:@"state"];
    NSLog(@"Login button value %@" , userState);

   
        if ([userState isEqualToString:@"visiter"]) {
            UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Access Denied"
                                                               message:@"Please Login first"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            
            [alertView show];
            
            [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
        }else{
    
    
    
    
    
    _profileImage.layer.cornerRadius = _profileImage.frame.size.width / 5;
    _profileImage.clipsToBounds = YES;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _scroll_View.contentSize = CGSizeMake(self.view.frame.size.width, _btn_save.frame.origin.y+_btn_save.frame.size.height + 10);
      
    // getting an NSString
    
    NSString *uid = [prefs stringForKey:@"uid"];
    
    NSLog(@"Uer id is %@" , uid);
    
    // login services code
    NSString *urlstr = @"http://mantis.vu.edu.pk/nextdoor/updateprofile/user/";
    NSString *json = @".json";
    NSLog(@"Array data is : %@" , uid);
    NSString *joinString=[NSString stringWithFormat:@"%@%@%@",urlstr,uid,json];
    
    NSLog(@"why this kola vary kola d %@" , joinString);
    NSURL *url = [NSURL URLWithString:joinString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             {
                                                 NSLog(@"Users API Response: %@", JSON);
                                                 responseDict = (NSDictionary*)JSON;
                                                 NSString *userName = [NSString stringWithString:[responseDict valueForKey:@"name"]];
                                                 NSString *email = [NSString stringWithString:[responseDict valueForKey:@"mail"]];
                                                 
                                                 NSDictionary *field_mobile_no =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_mobile_no_"]];
                                                 NSMutableArray *und_array = [[NSMutableArray alloc] init];
                                                 
                                                 for (NSDictionary *objectDict in [field_mobile_no objectForKey:@"und"]){
                                                 NSLog(@"objectdictionary .... %@" , objectDict);
                                                        [und_array addObject:objectDict];
                                                 
                                                    }
                                                NSDictionary *prinitngDict = [und_array objectAtIndex:0];
                                                                                        
                                                NSString *mobile = [prinitngDict valueForKey:@"value"];
                                                
                                            
                                                 // Code for First Name of User
                                                 
                                                 NSDictionary *field_firstName =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_first_name"]];
                                                 NSMutableArray *und_name_array = [[NSMutableArray alloc] init];
                                                 
                                                 for (NSDictionary *objectDict in [field_firstName objectForKey:@"und"]){
                                                     NSLog(@"objectdictionary .... %@" , objectDict);
                                                     [und_name_array addObject:objectDict];
                                                     
                                                 }
                                                 NSDictionary *name_index = [und_name_array objectAtIndex:0];
                                                 
                                                 NSString *first_name = [name_index valueForKey:@"value"];
                                                
                                                 NSLog(@"first Name %@" , first_name);
                                                
                                                 
                                                 // Code for Last Name of User
                                                 
                                                 NSDictionary *field_lasttName =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_last_name"]];
                                                 NSMutableArray *und_lastName_array = [[NSMutableArray alloc] init];
                                                 
                                                 for (NSDictionary *objectDict in [field_lasttName objectForKey:@"und"]){
                                                     NSLog(@"objectdictionary .... %@" , objectDict);
                                                     [und_lastName_array addObject:objectDict];
                                                     
                                                 }
                                                 
                                                 
                                                     NSDictionary *lastName_index = [und_lastName_array objectAtIndex:0];
                                                     
                                                     NSString *last_name = [lastName_index valueForKey:@"value"];
                                                     
                                                     NSLog(@"first Name %@" , last_name);
                                                 
                                                     
                                                 NSMutableArray *image_Array =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"picture"]];
                                                 NSString *imageURL = [image_Array valueForKey:@"url"];
                                                 NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
                                                 
                                                     [_img_profile setImage:[UIImage imageWithData:data]];
                                                 
                                                 
                                                 
                                                     _lbl_userName.text = userName;
                                                     _txt_email.text = email;
                                                     _txt_mobileNumber.text = mobile;
                                                    _txt_lastName.text = last_name;
                                                 
                                                 
                                                 
                                                
                                                 _txt_firstName.text = first_name;
                                                 
                                                 
                                             }
                                             
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 
                                                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                     message:[error localizedDescription]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"Ok"
                                                                                           otherButtonTitles:nil];
                                                 [alertView show];
                                                 
                                                 
                                             }];
    
    [jsonOperation start];

    
    
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
            
        }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//------fields show up when keybord clicks---------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self animateTextField:textField up:NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 130; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


//- (BOOL) textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

//---------end of fields showup code

- (IBAction)onClick_btn_save:(id)sender {
   
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    
    
    NSString *uid = [prefs stringForKey:@"uid"];
    NSString *password = [prefs stringForKey:@"pass"];
    NSString *sessid = [prefs stringForKey:@"sessid"];
    NSString *session_name = [prefs stringForKey:@"session_name"];
    NSString *token = [prefs stringForKey:@"token"];

    
    
    
    NSDictionary *paramDic = @{
        
        @"name":_lbl_userName.text,
        @"current_pass":password,
        @"mail":_txt_email.text,
        
        @"field_mobile_no_":@{
            @"und":@[
                   @{
                       @"value":_txt_mobileNumber.text
                   }
                   ]
        },
        
        @"field_first_name":@{
            @"und":@[
                   @{
                       @"value":_txt_firstName.text
                   }
                   ]
        },
        @"field_last_name":@{
            @"und":@[
                   @{
                       @"value":_txt_lastName.text
                   }
                   ]
        }
        
        };
    
        
    NSString *session = [NSString stringWithFormat:@"%@"@"="@"%@" , session_name , sessid];
    
    // if(token.length ==0){
    
    NSString *urlstr = @"http://mantis.vu.edu.pk/nextdoor/updateprofile/user/";
    NSString *json = @".json";
    NSLog(@"Array data is : %@" , uid);
    NSString *joinString=[NSString stringWithFormat:@"%@%@%@",urlstr,uid,json];
    
    
    NSMutableURLRequest *aRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:joinString]];
    
    //    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:aRequest delegate:self];
    NSError *error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:paramDic
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    [aRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [aRequest addValue:token forHTTPHeaderField:@"X-CSRF-Token"];
    [aRequest addValue:session forHTTPHeaderField:@"session"];
    [aRequest setHTTPMethod:@"PUT"];
    [aRequest setHTTPBody:dataFromDict];
    
    NSLog(@"http request is %@",aRequest);
    
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:aRequest delegate:self];
    
    NSLog(@"conn %@",conn);
    
    
    
    [conn start];
    
    UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Profile Updated"
                                                       message:@"Profile is Updated Successfully!!!!"
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    
    [alertView show];

    
//    EditProfileVC *tabVC = (TabVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"EditProfileVC"];
//    [self.navigationController pushViewController:tabVC animated:YES];
}
- (IBAction)btn_ocClick_selectPhoto:(id)sender {
    
    _picker = [[UIImagePickerController alloc] init];
    _picker.delegate = self;
    _picker.allowsEditing = YES;
    _picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    // _txt_ideaImage.text = @"image";
    //    txt_ideaImage.text = picker.sourceType;
    [self presentViewController:_picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //Or you can get the image url from AssetsLibrary
    path = [info valueForKey:UIImagePickerControllerReferenceURL];
    NSLog(@"%@",path);
    [picker dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)btn_onClick_uploadPhoto:(id)sender {
    
    self.profileImage.image = image;
        
    
}
@end
