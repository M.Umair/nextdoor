//
//  RoomDataVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 12/08/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostViewController.h"



@interface RoomDataVC : UIViewController<FillItDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic,strong) id<FillItDelegate> FillItDelegate;
- (IBAction)btn_onclick_SaveRoom:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_room_Number;
@property (strong, nonatomic) IBOutlet UITextField *txt_floor_Number;
@property (strong, nonatomic) IBOutlet UITextField *txt_room_type;
@property (strong, nonatomic) IBOutlet UITextField *txt_room_rent;
@property (strong, nonatomic) IBOutlet UITextField *txt_bed_rent;
@property (strong, nonatomic) IBOutlet UITextField *txt_bed_avaliable;
@property (strong, nonatomic) UIImagePickerController *picker;
- (IBAction)onClick_btn_selectImage:(id)sender;
- (IBAction)btn_uploadImage:(id)sender;


@property (strong, nonatomic) IBOutlet UIImageView *ImageView;



@property (strong, nonatomic) IBOutlet UIScrollView *sv_roomData;
@property (strong, nonatomic) IBOutlet UIButton *btn_roomSave;

@end
