//
//  PropertyDetailVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PropertyDetailVC : UIViewController<MKMapViewDelegate , CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIButton *btn_Appointment;
@property (weak, nonatomic) IBOutlet UIScrollView *scl_detail;

@property (strong, nonatomic) IBOutlet UILabel *lbl_property_status;

@property (strong, nonatomic) IBOutlet UILabel *lbl_property_type;
@property (strong, nonatomic) IBOutlet UILabel *lbl_property_id;

@property (strong, nonatomic) IBOutlet UILabel *lbl_property_area;
@property (strong, nonatomic) IBOutlet UILabel *lbl_beds;
@property (strong, nonatomic) IBOutlet UILabel *lbl_bath;
@property (strong, nonatomic) IBOutlet UILabel *lbl_garage;
@property (strong, nonatomic) IBOutlet UILabel *lbl_room_num;
@property (strong, nonatomic) IBOutlet UILabel *lbl_room_rent;
@property (strong, nonatomic) IBOutlet UILabel *lbl_room_status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_room_type;
@property (strong, nonatomic) IBOutlet UILabel *lbl_floor_num;
@property (strong, nonatomic) IBOutlet UILabel *lbl_property_body;
@property (strong, nonatomic) IBOutlet UILabel *lbl_bed_rent;
@property (strong, nonatomic) IBOutlet UILabel *lbl_bed_status;
@property (strong, nonatomic) IBOutlet UILabel *lbl_parking_type;
@property (strong, nonatomic) IBOutlet UILabel *lbl_apliances;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feature1;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feature2;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feature3;
@property (strong, nonatomic) IBOutlet UILabel *lbl_feature4;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Post_avalible;
@property (strong, nonatomic) IBOutlet MKMapView *view_map;




@property (nonatomic, strong) NSString *nodeID;


@end
