//
//  PropertyDetailVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "PropertyDetailVC.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"

@interface PropertyDetailVC () <MKMapViewDelegate>{
    __weak IBOutlet MKMapView *mapView;
    NSMutableArray *responseArray;
}
@property (strong, nonatomic) IBOutlet UIImageView *img_detail1;
@property (strong, nonatomic) IBOutlet UIImageView *img_detail2;
@property (strong, nonatomic) IBOutlet UIImageView *img_detail3;
@property (strong, nonatomic) IBOutlet UIImageView *img_detail4;
@end

@implementation PropertyDetailVC

@synthesize mapView;

CLLocationManager *locationManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}




-(void)One{
    [_img_detail1 setAlpha:1];
    [_img_detail2 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_img_detail2 setAlpha:1];
    [_img_detail1 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(Two) withObject:nil afterDelay:2.5];
    
}
-(void)Two{
    
    [_img_detail2 setAlpha:1];
    [_img_detail3 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_img_detail3 setAlpha:1];
    [_img_detail2 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(Three) withObject:nil afterDelay:2.5];}
-(void)Three{
    
    [_img_detail3 setAlpha:1];
    [_img_detail4 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_img_detail4 setAlpha:1];
    [_img_detail3 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(Four) withObject:nil afterDelay:2.5];
}

-(void)Four{
    
    [_img_detail4 setAlpha:1];
    [_img_detail1 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_img_detail1 setAlpha:1];
    [_img_detail4 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(One) withObject:nil afterDelay:2.5];
}


- (void)viewDidLoad
{

    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [_img_detail2 setAlpha:0];
    [_img_detail3 setAlpha:0];
    [_img_detail4 setAlpha:0];
    [_img_detail1 setAlpha:1];
    [self performSelector:@selector(One) withObject:nil afterDelay:2];
    
    _scl_detail.contentSize = CGSizeMake(self.view.frame.size.width, _btn_Appointment.frame.origin.y +_btn_Appointment.frame.size.height+ 10);
    
    _btn_Appointment.layer.cornerRadius=7;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    
    NSString *uid = [prefs stringForKey:@"uid"];
    
    NSString *urlString = [NSString stringWithFormat:@"http://mantis.vu.edu.pk/nextdoor/postAdd/node/%@.json" , uid];
    
    
    NSURL *url = [NSURL URLWithString:urlString];

    
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             
                                             {
                                                 
                                              NSDictionary *responseDict = (NSDictionary *)JSON;
                                                 
                                                 
                                                 
                                                 NSLog(@"response is ################ %@",responseDict);
                                                 
                                                 responseDict = (NSDictionary *)JSON;
                                                 
                                                 
                                                 
                                                 NSDictionary *field_property_status =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_property_status"]];
                                                                                             
                                                // NSLog(@"property status %@" , field_property_status);
                                                 
                                                 NSDictionary *objectDict = [field_property_status objectForKey:@"und"];
                                                  //NSLog(@"objectdictionary .... %@" , objectDict);
                                                 
                                                 NSArray *status =[objectDict valueForKey:@"tid"];
                                                // NSLog(@"value of tid %@", status);
                                                 
                                                 NSString *strStatus = [status objectAtIndex:0];
                                             //NSLog(@"value of tid %@", strStatus);
                                                 if([strStatus isEqualToString:@"12" ]){
                                                     //NSLog(@"For share");
                                                     _lbl_property_status.text = @"For Share";
                                                 }else if ([strStatus isEqualToString:@"13" ]){
                                                    // NSLog(@"For Rent");
                                                     _lbl_property_status.text = @"For Rent";
                                                 }
                                                 
                                                 //=======================================================
                                                 // Field_property_type
                                                 //=======================================================
                                                 
                                                 NSDictionary *field_property_type =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_property_type"]];
                                                 
                                                 //NSLog(@"property type %@" , field_property_type);
                                                 
                                                 NSDictionary *propertyobj = [field_property_type objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , propertyobj);
                                                NSArray *type = [propertyobj valueForKey:@"tid"];
                                                 //NSLog(@"value of property type %@" , type);
                                                  NSString *strPropertyType = [type objectAtIndex:0];
                                                 
                                                 if([strPropertyType isEqualToString:@"22" ]){
                                                     //NSLog(@"Hostel");
                                                     _lbl_property_type.text = @"Hostel";
                                                 }else if ([strPropertyType isEqualToString:@"3" ]){
                                                     //NSLog(@"Apartment");
                                                     _lbl_property_type.text = @"Apartment";
                                                 }

                                                 //=======================================================
                                                 // Field_property_discription
                                                 //=======================================================
                                                 
                                                                                                 
//                                                 NSDictionary *field_description =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"body"]];
//
//                                                 //NSLog(@"property type %@" , field_description);
//                                                 
//                                                 NSDictionary *desobj = [field_description objectForKey:@"und"];
//                                                // NSLog(@"objectdictionary .... %@" , desobj);
//                                                 
//                                                 NSArray *desc =[desobj valueForKey:@"value"];
//                                                 //NSLog(@"Description %@" , desc);
//                                                 
//                                                 NSString  *strDiscription = [desc objectAtIndex:0];
//                                                 _lbl_property_body.text=strDiscription;
                                                 
                                                 
                                                 //=======================================================
                                                 // Field_property_id
                                                 //=======================================================
                                                 
                                                 
                                                 NSDictionary *field_id =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_property_id"]];
                                                 
                                                 //NSLog(@"property type %@" , field_id);
                                                 
                                                 NSDictionary *idObj = [field_id objectForKey:@"und"];
                                                // NSLog(@"objectdictionary .... %@" , idObj);
                                                 
                                                 NSArray *id =[idObj valueForKey:@"value"];
                                                // NSLog(@"Property ID is %@" , id);
                                                 NSString *strId = [id objectAtIndex:0];
                                                 _lbl_property_id.text=strId;
                                                 
                                                 //=======================================================
                                                 // Field_property_area
                                                 //=======================================================/

                                                 NSDictionary *field_property_area =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_city_and_area"]];
                                                 
                                                 //NSLog(@"property type %@" , field_property_area);
                                                 
                                                 NSDictionary *areaObj = [field_property_area objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , areaObj);
                                                 
                                                 NSArray *area =[areaObj valueForKey:@"tid"];
                                                //NSLog(@"Property area is %@" , area);
                                                 NSString *strArea = [area objectAtIndex:0];
                                                 
                                                 if([strArea isEqualToString:@"23" ]){
                                                     //NSLog(@"Hostel");
                                                     _lbl_property_area.text = @"Lahore";
                                                 }else if ([strArea isEqualToString:@"24" ]){
                                                     //NSLog(@"Apartment");
                                                     _lbl_property_area.text = @"Ichra";
                                                 }else if ([strArea isEqualToString:@"25" ]){
                                                     //NSLog(@"Apartment");
                                                     _lbl_property_area.text = @"Johor Town";
                                                 }else if ([strArea isEqualToString:@"26" ]){
                                                     //NSLog(@"Apartment");
                                                     _lbl_property_area.text = @"Model Town";
                                                 }else if ([strArea isEqualToString:@"27" ]){
                                                     //NSLog(@"Apartment");
                                                     _lbl_property_area.text = @"Gulburg";
                                                 }

                                                 //=======================================================
                                                 // Field_property_bed
                                                 //=======================================================
                                                 
                                                 
                                                 NSDictionary *field_beds =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_beds"]];
                                                 
                                                 //NSLog(@"property beds %@" , field_beds);
                                                 
                                                 NSDictionary *bedObj = [field_beds objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , bedObj);
                                                 
                                                 NSArray *beds =[bedObj valueForKey:@"value"];
                                                 //NSLog(@"Property Beds %@" , beds);
                                                 
                                                 NSString *strBed = [beds objectAtIndex:0];
                                                 _lbl_beds.text=strBed;
                                                 
                                                 //=======================================================
                                                 // Field_property_bathroom
                                                 //=======================================================                                                
                                                 
                                                 NSDictionary *field_bathrooms =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_bathrooms"]];
                                                 
                                                 //NSLog(@"property bathrooms %@" , field_bathrooms);
                                                 
                                                 NSDictionary *bathObj = [field_bathrooms objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , bathObj);
                                                 
                                                 NSArray *bath =[bathObj valueForKey:@"value"];
                                                 //NSLog(@"Property Bath is %@" , bath);
                                                 NSString *strBathroom = [bath objectAtIndex:0];
                                                 _lbl_bath.text=strBathroom;
                                                 
                                                 //=======================================================
                                                 // Field_property_garage
                                                 //=======================================================
                                                 

                                                 NSDictionary *field_garage =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_garage"]];
                                                 
                                                 //NSLog(@"property garages %@" , field_garage);
                                                 
                                                 NSDictionary *garageObj = [field_garage objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , garageObj);
                                                 
                                                 NSArray *garage =[garageObj valueForKey:@"value"];
                                                // NSLog(@"Property garage is %@" , garage);
                                                 NSString *strGarage =[garage objectAtIndex:0];
                                                 _lbl_garage.text = strGarage;
                                                 
                                                 //=======================================================
                                                 // Field_property_appliances
                                                 //=======================================================
                                                 
                                                 
                                                 NSDictionary *field_appliances =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_appliances"]];
                                                 
                                                 //NSLog(@"field_appliances %@" , field_appliances);
                                                 
                                                 NSDictionary *appliancesObj = [field_appliances objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , appliancesObj);
                                                 
                                                 NSArray *appliances =[appliancesObj valueForKey:@"value"];
                                                 // NSLog(@"Property appliances is %@" , appliances);
                                                
                                                 //NSLog(@"appliances count %i", appliances.count);
                                                 NSString *strAppliances ;
                                                 NSString *strAppliances1=@"";
                                                 for (int i=0; i<appliances.count; i++) {
                                                     
                                                    strAppliances =[appliances objectAtIndex:i]; 
                                                   strAppliances1= [strAppliances1 stringByAppendingString:strAppliances];
                                                     
                                                     //NSLog(@"Property appliances is %@" , strAppliances1);
                                                 }
                                                 
                                                  _lbl_apliances.text = strAppliances1;
                                                 
                                                 
                                                 
                                                 //=======================================================
                                                 // Field_property_Parking
                                                 //=======================================================
                                                 
                                                 
                                                 NSDictionary *field_parking =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_parking_type"]];
                                                 
                                                 //NSLog(@"field_appliances %@" , field_appliances);
                                                 
                                                 NSDictionary *parkingObj = [field_parking objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , appliancesObj);
                                                 
                                                 NSArray *parking =[parkingObj valueForKey:@"value"];
                                                 //NSLog(@"Property appliances is %@" , appliances);
                                                 
                                                 //NSLog(@"appliances count %i", appliances.count);
                                                 NSString *strparking ;
                                                 NSString *strparking1=@"";
                                                 for (int i=0; i<parking.count; i++) {
                                                     
                                                     strparking =[parking objectAtIndex:i];
                                                     strparking1= [strparking1 stringByAppendingString:strparking];
                                                     
                                                    // NSLog(@"Property appliances is %@" , strparking1);
                                                 }
                                                 
                                                 _lbl_parking_type.text = strparking1;
                                                 
                                                 
                                                 
                                                 //=======================================================
                                                 // Field_property_avalibility
                                                 //=======================================================
                                                 
                                                 NSDictionary *field_availability =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_post_availability_"]];
                                                 
                                                 //NSLog(@"property type %@" , field_property_area);
                                                 
                                                 NSDictionary *availabilityObj = [field_availability objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , areaObj);
                                                 
                                                 NSArray *availability =[availabilityObj valueForKey:@"value"];
                                                 //NSLog(@"Property area is %@" , area);
                                                 NSString *stravailability= [availability objectAtIndex:0];
                                                 
                                                 if([stravailability isEqualToString:@"0" ]){
                                                     //NSLog(@"Hostel");
                                                     _lbl_Post_avalible.text = @"Unavailable";
                                                 }else if ([stravailability isEqualToString:@"1" ]){
                                                     //NSLog(@"Apartment");
                                                     _lbl_Post_avalible.text = @"Avaliable";
                                                 }
                                                 /////////////////////////////////////
                                                 
                                                 NSDictionary *field_features =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_features"]];
                                                 
                                                 //NSLog(@"field_appliances %@" , field_appliances);
                                                 
                                                 NSDictionary *featuresObj = [field_features objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , appliancesObj);
                                                 
                                                 NSArray *featues =[featuresObj valueForKey:@"value"];
                                                 // NSLog(@"Property appliances is %@" , appliances);
                                                 
                                                 //NSLog(@"appliances count %i", appliances.count);
                                                 NSString *strfeatures ;
                                                 NSString *strfeatures1=@"";
                                                 for (int i=0; i<featues.count; i++) {
                                                     
                                                     strfeatures =[featues objectAtIndex:i];
                                                     NSString *featuresVal;
                                                     if ([strfeatures isEqualToString:@"1"]) {
                                                         featuresVal = @"2 Stories";
                                                     }else if([strfeatures isEqualToString:@"2"]){
                                                         featuresVal = @" Central Heating";
                                                     
                                                     }else if([strfeatures isEqualToString:@"3"]){
                                                         featuresVal = @" Electric Range";
                                                     }else if([strfeatures isEqualToString:@"4"]){
                                                         featuresVal = @" Fire Place";
                                                     }else if([strfeatures isEqualToString:@"5"]){
                                                         featuresVal = @" Home Theatre";
                                                     }else if([strfeatures isEqualToString:@"6"]){
                                                         featuresVal = @" Ac In Room";
                                                     }else if([strfeatures isEqualToString:@"7"]){
                                                         featuresVal = @" LAWN";
                                                     }else if([strfeatures isEqualToString:@"8"]){
                                                         featuresVal = @" Marble Floors";
                                                     }


                                                     strfeatures1= [strfeatures1 stringByAppendingString:featuresVal];
                                                     
                                                     _lbl_feature1.text = strfeatures1;
                                                     
                                                     //NSLog(@"Property appliances is %@" , strAppliances1);
                                                 }
                                                 
                                                 ////// location things
                                                 
                                                 NSDictionary *field_location =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_property_location"]];
                                                 
                                                 //NSLog(@"field_appliances %@" , field_appliances);
                                                 
                                                 NSDictionary *location_und = [field_location objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , appliancesObj);
                                                 
                                                 NSArray *latitude =[location_und valueForKey:@"latitude"];
                                                 
                                                 NSString *latitude_index = [latitude objectAtIndex:0];
                                                 
                                                 float eX1rounded = [latitude_index floatValue];
                                            
                                                 NSArray *longitude =[location_und valueForKey:@"longitude"];
                                                 NSString *longitude_index = [longitude objectAtIndex:0];
                                                 
                                                 float flt_longitude = [longitude_index floatValue];
                                                 
                                                 
                                                 NSLog(@"latituide of location %f" , eX1rounded);
                                                 NSLog(@"latituide of location %f" , flt_longitude);
//                                                 
                                                                                                
                                                 
                                                 //=======================================================
                                                 // Field_property_image
                                                 //=======================================================
                                                 
                                                 NSDictionary *field_image =[NSDictionary dictionaryWithDictionary:[responseDict valueForKey:@"field_property_image"]];
                                                 
                                                 //NSLog(@"property type %@" , field_property_area);
                                                 
                                                 NSDictionary *imageObj = [field_image objectForKey:@"und"];
                                                 //NSLog(@"objectdictionary .... %@" , areaObj);
                                                 
                                                 NSArray *property_image=[imageObj valueForKey:@"filename"];
                                                 //NSLog(@"Property area is %@" , area);
                                                 NSLog(@"imagesssss..........%@" , property_image);
                                                 for (int i=0; i<property_image.count; i++) {
                                                     NSString *strimage= [property_image objectAtIndex:i];
                                                    
                                                     NSString *imagepath = [NSString stringWithFormat:@"http://mantis.vu.edu.pk/nextdoor/sites/default/files/styles/property-full-image/public/property-image/%@" , strimage];
                                                      NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imagepath]];
                                                     if(i==0){
                                                         [_img_detail1 setImage:[UIImage imageWithData:data]];
                                                     }else if (i==1){
                                                         [_img_detail2 setImage:[UIImage imageWithData:data]];
                                                     }else if (i==2){
                                                         [_img_detail3 setImage:[UIImage imageWithData:data]];
                                                     }else if (i==3){
                                                         [_img_detail4 setImage:[UIImage imageWithData:data]];
                                                     }
                                                 }
                                                 
                                                 
                                             }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 
                                                 
                                                 
                                                 
                                                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                     message:[error localizedDescription]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"Ok"
                                                                                           otherButtonTitles:nil];
                                                 [alertView show];
                                                 
                                                 
                                             }];
    
    [jsonOperation start];

    mapView.showsUserLocation = YES;
    locationManager = [CLLocationManager new];
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        //[locationManager requestWhenInUseAuthorization];
    }

    
    
    
}






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
