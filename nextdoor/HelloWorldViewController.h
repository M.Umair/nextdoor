//
//  HelloWorldViewController.h
//  nextdoor
//
//  Created by faiza zahid on 30/06/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldViewController : UIViewController {
    NSArray *titles;
}
@property(nonatomic, assign)NSArray *titles;

@property (strong, nonatomic) IBOutlet UIBarButtonItem *btn_login;

@property (nonatomic , strong) IBOutlet UIImageView *profilepic;
- (IBAction)onClick_btn_loginLogout:(id)sender;

@end
