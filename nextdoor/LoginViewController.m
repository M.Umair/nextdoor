//
//  LoginViewController.m
//  nextdoor
//
//  Created by Muhammad Usman on 15/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "LoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TabVC.h"
#import "RegisterViewController.h"
#import "UserProfileVC.h"
#import "ForgotPassVC.h"
#import "AFNetworking.h"
#import "PostViewController.h"

@interface LoginViewController ()
{
    NSDictionary *responseDict;
    NSString *loginSession;
}
@end

@implementation LoginViewController


//@synthesize sessid;
//@synthesize session_name;
//@synthesize token;

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _btn_login.layer.cornerRadius = 5;
    _btn_glogin.layer.cornerRadius = 5;
    _btn_fblogin.layer.cornerRadius = 5;
    _btn_caguest.layer.cornerRadius = 5;
    
   //byDefualt Value of loginSession
    
    
    
    
//    PostViewController *postviewcontroller = [[PostViewController alloc]init];
    
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
    
    
    
}

//------fields show up when keybord clicks---------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self animateTextField:textField up:NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 20; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


//- (BOOL) textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

//---------end of fields showup code

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClick_btn_Continue:(id)sender {
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // saving an NSString
    loginSession = @"visiter";
    
    [prefs setObject:loginSession forKey:@"state"];
    
    TabVC *tabVC = (TabVC *)  [self.storyboard instantiateViewControllerWithIdentifier:@"TabVC"];
    [self.navigationController pushViewController:tabVC animated:YES];
}

- (IBAction)onClick_btn_SignUP:(id)sender
{
    RegisterViewController *sighUP = (RegisterViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:sighUP animated:YES];
}

- (IBAction)onClick_btn_Login:(id)sender {
    
    
    [self.view endEditing:YES];
    
//    UserProfileVC *userLogin= (UserProfileVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
//    [self.navigationController pushViewController:userLogin animated:YES];
    if ( ([_txt_username.text length] != 0) && ([_txt_password.text length] != 0)){
    
    
    
     // login services code
    NSURL *url = [NSURL URLWithString:@"http://mantis.vu.edu.pk"];
    
    NSDictionary *paramDict = [NSDictionary dictionaryWithObjectsAndKeys:_txt_username.text,@"username",
                               _txt_password.text,@"password",
                               nil];
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSMutableURLRequest *request = [client requestWithMethod:@"POST"
                                                 path:@"/nextdoor/note/user/login.json"
                                           parameters:paramDict];
     AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             {
                                                 
                                                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                                                 
                                                 // saving an NSString
                                                 loginSession = @"user";
                                                 
                                                 [prefs setObject:loginSession forKey:@"state"];
                                                 
                                                 
                                                 
                                                 TabVC *tabVC = (TabVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"TabVC"];
                                                 [self.navigationController pushViewController:tabVC animated:YES];
                                                 
                                                 NSLog(@"Users API Response: %@", JSON);
                                                 responseDict = (NSDictionary*)JSON;
                                                 NSString *sessid = [NSString stringWithString:[responseDict valueForKey:@"sessid"]];
                                                 NSString *session_name = [NSString stringWithString:[responseDict valueForKey:@"session_name"]];
                                                 NSString *token = [NSString stringWithString:[responseDict valueForKey:@"token"]];
                                                 
                                                 
                                                 NSUserDefaults *userdata = [NSUserDefaults standardUserDefaults];
                                                 
                                                 // saving an NSString
                                                 
                                                 [userdata setObject:sessid forKey:@"sessid"];
                                                 [userdata setObject:session_name forKey:@"session_name"];
                                                 [userdata setObject:token forKey:@"token"];
                                                 [userdata synchronize];
                                                 
            NSDictionary *dataArray = [NSDictionary dictionaryWithDictionary:[responseDict objectForKey:@"user"]];
                                      
                                                 
                                                 NSDictionary *field_mobile_no =[NSDictionary dictionaryWithDictionary:[dataArray valueForKey:@"field_mobile_no_"]];
                                                 
                                                 NSLog(@"post status data.....%@" , field_mobile_no );
                                                 
                                                 
                                                 NSMutableArray *und_array = [[NSMutableArray alloc] init];
                                                 
                                                 for (NSDictionary *objectDict in [field_mobile_no objectForKey:@"und"])
                                                 {
                                                     NSLog(@"objectdictionary .... %@" , objectDict);
                                                     [und_array addObject:objectDict];
                                                     
                                                 }
                                                 
                                                 NSLog(@"First Index of data array is %@",[und_array objectAtIndex:0]);
                                                 
                                                 NSDictionary *prinitngDict = [und_array objectAtIndex:0];
                                                 
                                                 NSString *mobile = [prinitngDict valueForKey:@"value"];
                                                 NSLog(@"Mobile number is : %@" , mobile);
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 NSLog(@"User Data %@" , dataArray);
                                                 NSString *uid = [NSString stringWithString:[dataArray objectForKey:@"uid"]];
                                                 NSString *name = [NSString stringWithString:[dataArray objectForKey:@"name"]];
                                                 
                                                 NSString *mail = [NSString stringWithString:[dataArray objectForKey:@"mail"]];
                                                 
                                                 // saving an NSString
                                                
                                                 [prefs setObject:_txt_password.text forKey:@"pass"];
                                                 [prefs setObject:uid forKey:@"uid"];
                                                 [prefs setObject:mail forKey:@"mail"];
                                                 [prefs setObject:name forKey:@"name"];
                                                 [prefs setObject:mobile forKey:@"mobile_no"];
                                                 [prefs synchronize];
                                                 //NSLog(@"User Data %@" , uid);
                                                 

                                                 
                                                                                              }
                                             
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 
                                                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                     message:[error localizedDescription]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"Ok"
                                                                                           otherButtonTitles:nil];
                                                 [alertView show];
                                                 
                                                 
                                             }];
    
    [jsonOperation start];
    
    }else{
    
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Missing required data"
                                                           message:@"First enter email and password"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        
        [alertView show];
    
    }
    //sessions
    
}

- (IBAction)onClick_btn_forgotPass:(id)sender {
    
    ForgotPassVC *password = (ForgotPassVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPassVC"];
    [self.navigationController pushViewController:password animated:YES];
}
@end
