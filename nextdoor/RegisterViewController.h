//
//  RegisterViewController.h
//  nextdoor
//
//  Created by Muhammad Usman on 15/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btn_createAccount;
@property (strong, nonatomic) IBOutlet UITextField *txt_mail;
@property (strong, nonatomic) IBOutlet UITextField *txt_mobile;

- (IBAction)btn_createAccount:(UIButton *)sender;


@end
