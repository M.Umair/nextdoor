//
//  InboxVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 25/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "InboxVC.h"
#import "ListingCell.h"
@interface InboxVC ()

@end

@implementation InboxVC{
    NSArray *tableData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    tableData = [NSArray arrayWithObjects:@"Usman", @"Umair (MUAKH)", @"Faiza Zahid", @"Amir", @"Maria", @"Aysha Arooj", @"Hafiz Wajahat Hashmi", @"Qasim Ali", @"Saad Sadique", @"Saed Ahmad", @"Ayesha Anjum", @"Faisal Ali", @"Shahla Kiran", @"Muqadas Zahra",nil];
    
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return tableData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellidentifier = @"Cell";
    ListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath] ;
    
    cell.inboxcell_username_lbl.text = [tableData objectAtIndex:indexPath.row];
    //cell.propertystatuslbl.text = [propertyStatus objectAtIndex:indexPath.row];
    //cell.propImage.image = [UIImage imageNamed:[pics objectAtIndex:indexPath.row]];
    
    //cell.propImage.layer.cornerRadius = cell.propImage.frame.size.width / 2;
    //cell.propImage.clipsToBounds = YES;
    return cell;
}

@end
