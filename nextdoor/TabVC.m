//
//  TabVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 28/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "TabVC.h"

@interface TabVC ()

@end

@implementation TabVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.moreNavigationController.topViewController.view.backgroundColor =[UIColor colorWithRed: 0.83 green:0.83 blue:0.83 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
