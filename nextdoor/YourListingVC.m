//
//  YourListingVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "YourListingVC.h"
#import "ListingCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
#import "PropertyDetailVC.h"
@interface YourListingVC ()
{
    
    NSMutableArray *responseArray;
    
    NSDictionary *responseDict;
    NSData *responseDicti;
    
}

@end

@implementation YourListingVC{
    NSArray *tableData;
    NSArray *pics;
    NSArray *time;
}
@synthesize tbl_View;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *userState =[prefs stringForKey:@"state"];
    NSLog(@"Login button value %@" , userState);
    
    
    if ([userState isEqualToString:@"visiter"]) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Access Denied"
                                                           message:@"Please Login first"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        
        [alertView show];
        
        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
    }else{
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    
    NSString *uid = [prefs stringForKey:@"uid"];
    NSString *urlString = [NSString stringWithFormat:@"http://mantis.vu.edu.pk/nextdoor/postAdd/node.json?parameters[type]=property&parameters[status]=1&parameters[uid]=%@",uid];
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    
    
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
                                             
                                             {
                                                 
                                                 responseDict = (NSDictionary *)JSON;
                                                 
                                                 
                                                 
                                                 //NSLog(@"response is ################ %@",responseDict);
                                                 
                                                 
                                                 
                                                 
                                                 
                                                 responseArray =[[NSMutableArray alloc] init];
                                                 
                                                 
                                                 for (NSDictionary *objects in responseDict)
                                                 {
                                                     
                                                     
                                                     [responseArray addObject:objects];
                                                     
                                                 }
                                                 
                                                 [tbl_View reloadData];
                                                 
                                                 
                                                 
                                                 
                                                 
                                             }
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 
                                                 
                                                 
                                                 
                                                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                     message:[error localizedDescription]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"Ok"
                                                                                           otherButtonTitles:nil];
                                                 [alertView show];
                                                 
                                                 
                                             }];
    
    [jsonOperation start];
    
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    
    
    
    
    
    
    
    tableData = [NSArray arrayWithObjects:@"Usman", @"Umair (MUAKH)", @"Faiza Zahid", @"Amir", @"Maria", @"Aysha Arooj",nil];
    
    pics = [NSArray arrayWithObjects:@"2.jpg", @"3.jpg", @"4.jpg", @"5.jpg", @"6.jpg", @"7.jpg",nil];
    
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
//    return 1;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return tableData.count;
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    static NSString *cellidentifier = @"Cell";
//    ListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath] ;
//        cell.propertnamelbl.text = [tableData objectAtIndex:indexPath.row];
//        //cell.propertystatuslbl.text = [propertyStatus objectAtIndex:indexPath.row];
//        cell.propImage.image = [UIImage imageNamed:[pics objectAtIndex:indexPath.row]];
//    cell.propImage.layer.cornerRadius = cell.propImage.frame.size.width / 2;
//    cell.propImage.clipsToBounds = YES;
//
//        return cell;
//
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    
    return responseArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellidentifier = @"Cell";
    
    ListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath] ;
    
    NSDictionary *resultDict = [responseArray objectAtIndex:indexPath.row];
    
    cell.propertnamelbl.text = [resultDict valueForKey:@"title"];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *resultDict = [responseArray objectAtIndex:indexPath.row];
    
    
    PropertyDetailVC *propertyDetail = (PropertyDetailVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"PropertyDetailVC"];
    
    propertyDetail.nodeID = [resultDict valueForKey:@"nid"];
    
    NSString *nid = [resultDict valueForKey:@"nid"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // saving an NSString
    
    [prefs setObject:nid forKey:@"uid"];
    
    [prefs synchronize];
    [self.navigationController pushViewController:propertyDetail animated:YES];
    
    
    
}



@end
