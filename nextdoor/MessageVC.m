//
//  MessageVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 25/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "MessageVC.h"
#import <QuartzCore/QuartzCore.h>
#import "ListingCell.h"
@interface MessageVC ()

@end

@implementation MessageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellidentifier = @"Cell";
    //static NSString *cellidentifier2 = @"Cell2";
    ListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath] ;
    
   // cell.inboxcell_username_lbl.text = [tableData objectAtIndex:indexPath.row];
    //cell.propertystatuslbl.text = [propertyStatus objectAtIndex:indexPath.row];
    //cell.propImage.image = [UIImage imageNamed:[pics objectAtIndex:indexPath.row]];
    
    //cell.propImage.layer.cornerRadius = cell.propImage.frame.size.width / 2;
    //cell.propImage.clipsToBounds = YES;
    return cell;
}



@end
