//
//  ForgotPassVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 26/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "ForgotPassVC.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
#import "LoginViewController.h"
//#import "LoginViewController.h"

@interface ForgotPassVC ()

@end

@implementation ForgotPassVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _btn_mail.layer.cornerRadius=7;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

- (IBAction)onClick_btn_Email:(id)sender {

    
    if ( ([_txt_email.text length] != 0)){
        
        NSURL *url = [NSURL URLWithString:@"http://mantis.vu.edu.pk"];
        
        
        NSDictionary *parameterDict = [NSDictionary dictionaryWithObjectsAndKeys:_txt_email.text,@"name",nil];
        
        AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
        NSURLRequest *request = [client requestWithMethod:@"POST"
                                                     path:@"/nextdoor/note/user/request_new_password.json"
                                               parameters:parameterDict];
        
        AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
            
            NSLog(@"registered user id %@" , JSON);
            
            UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Forgot Password"
                                                               message:@"Please check your Email Account for new Password"
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            
            [alertView show];
            
            LoginViewController *loginviewontroller = (LoginViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [self.navigationController pushViewController:loginviewontroller animated:YES];
            
        }
                                                 
                                                                                                failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                                 {
                                                     
                                                     UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                         message:[error localizedDescription]
                                                                                                        delegate:nil
                                                                                               cancelButtonTitle:@"Ok"
                                                                                               otherButtonTitles:nil];
                                                     [alertView show];
                                                     
                                                     
                                                 }];
        
        [jsonOperation start];
    }else{
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Missing required data"
                                                           message:@"First enter email and mobile Number"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        
        [alertView show];
    }
    


}
@end
