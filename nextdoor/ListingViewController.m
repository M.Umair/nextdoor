//
//  ListingViewController.m
//  nextdoor
//
//  Created by Muhammad Usman on 15/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "ListingViewController.h"
#import "ListingCell.h"


@interface ListingViewController ()



@end

@implementation ListingViewController
{
    NSArray *tableData;
    NSArray *pics;
    NSArray *time;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    tableData = [NSArray arrayWithObjects:@"Usman", @"Umair (MUAKH)", @"Faiza Zahid", @"Amir", @"Maria", @"Aysha Arooj", @"Hafiz Wajahat Hashmi", @"Qasim Ali", @"Saad Sadique", @"Saed Ahmad", @"Ayesha Anjum", @"Faisal Ali", @"Shahla Kiran", @"Muqadas Zahra",nil];
    
    pics = [NSArray arrayWithObjects:@"2.jpg", @"3.jpg", @"4.jpg", @"5.jpg", @"6.jpg", @"7.jpg", @"8.jpg", @"9.jpg", @"10.jpg", @"11.jpg", @"12.jpg", @"13.jpg", @"14.jpg", @"15.jpg",nil];
    
//    time = [NSArray arrayWithObjects:@"Usman", @"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman",@"Usman", nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 78;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    ListingCell *cell = (ListingCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListingCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.nameLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.thumbnailImageView.image = [UIImage imageNamed:[pics objectAtIndex:indexPath.row]];
    //cell.prepTimeLabel.text = [time objectAtIndex:indexPath.row];
    
    return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
