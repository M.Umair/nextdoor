//
//  RegisterViewController.m
//  nextdoor
//
//  Created by Muhammad Usman on 15/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "RegisterViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    _btn_createAccount.layer.cornerRadius=5;
    
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
    
                                                 
    
}

//------fields show up when keybord clicks---------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self animateTextField:textField up:NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


//- (BOOL) textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

//---------end of fields showup code

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_createAccount:(UIButton *)sender {
    
    
    if ( ([_txt_mail.text length] != 0) && ([_txt_mobile.text length] != 0)){
    
    NSURL *url = [NSURL URLWithString:@"http://mantis.vu.edu.pk"];

    
    NSDictionary *paramDict = @{
                                
                                @"mail": _txt_mail.text,
                                @"field_mobile_no_": @{
                                        
                                        @"und": @[
                                                @{
                                                    @"value": _txt_mobile.text,
                                                    }
                                                ]
                                        }
                                };
    
    AFHTTPClient *client = [AFHTTPClient clientWithBaseURL:url];
    NSURLRequest *request = [client requestWithMethod:@"POST"
                                                 path:@"/nextdoor/note/user/register.json"
                                           parameters:paramDict];
    
    AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON){
        
        NSLog(@"registered user id %@" , JSON);
        
    }
                                             
                                                                                            failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                             {
                                                 
                                                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                     message:[error localizedDescription]
                                                                                                    delegate:nil
                                                                                           cancelButtonTitle:@"Ok"
                                                                                           otherButtonTitles:nil];
                                                 [alertView show];
                                                 
                                                 
                                             }];
    
    [jsonOperation start];
    }else{
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Missing required data"
                                                           message:@"First enter email and mobile Number"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        
        [alertView show];
    }

}
@end
