//
//  FillITVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 08/08/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ContactUsVC.h"



@interface FillITVC : UIViewController <FillItDelegate>

@property (nonatomic,strong) id<FillItDelegate> FillItDelegate;

@property (nonatomic, strong) NSString *iAmString;
@property (strong, nonatomic) IBOutlet UITextField *txt_Name;
@property (strong, nonatomic) IBOutlet UITextField *txt_Email;
@property (strong, nonatomic) IBOutlet UITextView *txt_Info;
@property (strong, nonatomic) IBOutlet UITextField *txt_Subject;
- (IBAction)onClick_btn_FillIT:(id)sender;

@end
