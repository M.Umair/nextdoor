//
//  ContactUsVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 26/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "ContactUsVC.h"
#import <QuartzCore/QuartzCore.h>
#import "FillITVC.h"



@class ContactUsVC;


@interface ContactUsVC ()
{
    NSMutableArray *roomsArray;
}
@end

@implementation ContactUsVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _btn_sendMsg.layer.cornerRadius=7;
    
     _contact_sv.contentSize = CGSizeMake(self.view.frame.size.width, _btn_sendMsg.frame.origin.y+_btn_sendMsg.frame.size.height + 150);
    
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    roomsArray = [[NSMutableArray alloc] init];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClick_send_Message:(id)sender
{
    
    FillITVC *FVC = (FillITVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"FillITVC"];
    
    FVC.FillItDelegate = self;
    
    [self.navigationController pushViewController:FVC animated:YES];
    
}



////Delegate Body
-(void) fillData:(NSDictionary *)contactUs
{
    
    
    [_btn_sendMsg setTitle:[contactUs valueForKey:@"name"] forState:UIControlStateNormal];
    
    
    NSLog(@"Dictionary is %@",contactUs);
    
    
    [roomsArray addObject:contactUs];
    
    
    NSLog(@"rooms are %@",roomsArray);
    
}

//------fields show up when keybord clicks---------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self animateTextField:textField up:NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 80; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


//- (BOOL) textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

//---------end of fields showup code



@end
