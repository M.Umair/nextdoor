//
//  ListingCell.m
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "ListingCell.h"

@implementation ListingCell


@synthesize propertnamelbl;
@synthesize propertystatuslbl;
@synthesize propImage;
@synthesize inboxcell_latestmessage_lbl;
@synthesize inboxcell_time_lbl;
@synthesize inboxcell_username_lbl;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    propertnamelbl.textColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0];    
}

@end
