//
//  ListingCell.h
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *propertnamelbl;
@property (weak, nonatomic) IBOutlet UILabel *propertystatuslbl;
@property (weak, nonatomic) IBOutlet UIImageView *propImage;

@property (weak, nonatomic) IBOutlet UILabel *inboxcell_username_lbl;
@property (weak, nonatomic) IBOutlet UILabel *inboxcell_latestmessage_lbl;
@property (weak, nonatomic) IBOutlet UILabel *inboxcell_time_lbl;

@property (weak, nonatomic) IBOutlet UIImageView *user_Image;
@property (weak, nonatomic) IBOutlet UILabel *messagelabl;

@property (weak, nonatomic) IBOutlet UIImageView *user2_Image;
@property (weak, nonatomic) IBOutlet UILabel *message2_lbl;

@end
