//
//  HelloWorldViewController.m
//  nextdoor
//
//  Created by faiza zahid on 30/06/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "HelloWorldViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "LoginViewController.h"
#import "AFNetworking.h"

@interface HelloWorldViewController ()
{
    NSMutableArray *usersArrayList;
    NSArray *responseDict;
    

}
@property (strong, nonatomic) IBOutlet UIImageView *SImage1;
@property (strong, nonatomic) IBOutlet UIImageView *SImage2;
@property (strong, nonatomic) IBOutlet UIImageView *SImage3;

@end


@implementation HelloWorldViewController

@synthesize titles =_titles;
-(void)One{
    [_SImage1 setAlpha:1];
    [_SImage2 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_SImage2 setAlpha:1];
    [_SImage1 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(Two) withObject:nil afterDelay:2.5];
    
}
-(void)Two{
    
    [_SImage2 setAlpha:1];
    [_SImage3 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_SImage3 setAlpha:1];
    [_SImage2 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(Three) withObject:nil afterDelay:2.5];}
-(void)Three{
    
    [_SImage3 setAlpha:1];
    [_SImage1 setAlpha:0];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:1];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [_SImage1 setAlpha:1];
    [_SImage3 setAlpha:0];
    [UIView commitAnimations];
    [self performSelector:@selector(One) withObject:nil afterDelay:2.5];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
     NSString *userState =[prefs stringForKey:@"state"];
    NSLog(@"Login button value %@" , userState);
    if([userState isEqualToString:@"user"]){
        _btn_login.title = @"Logout";
    }else{
        _btn_login.title = @"Login";
    }
    
	self.profilepic.layer.cornerRadius = self.profilepic.frame.size.width/2;
    self.profilepic.clipsToBounds = YES;
    
    [_SImage2 setAlpha:0];
    [_SImage3 setAlpha:0];
    [_SImage1 setAlpha:1];
    [self performSelector:@selector(One) withObject:nil afterDelay:2];
    
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.tabBarController.moreNavigationController.navigationBar.tintColor = [UIColor blackColor];
    
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClick_btn_loginLogout:(id)sender
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // saving an NSString
    NSString *loginSession = @"";
    
    [prefs setObject:loginSession forKey:@"pass"];
    
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    if ([_btn_login.title isEqualToString: @"Logout"]){
        for (NSHTTPCookie *cookie in [storage cookies]) {
            [storage deleteCookie:cookie];
        }
        
        _btn_login.title=@"Login";
        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
         
    }else{
        
         _btn_login.title=@"Logout";
        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
    }
    
}
@end
