//
//  PostViewController.h
//  nextdoor
//
//  Created by Muhammad Usman on 16/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol FillItDelegate <NSObject>

@optional

-(void) fillData:(NSDictionary *)contactUs;
@end



@interface PostViewController : UIViewController <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate , FillItDelegate>

{
    IBOutlet UITextField *txt_city;
    IBOutlet UITextField *txt_provience;
    IBOutlet UITextField *txt_area;
    IBOutlet UITextField *txt_parkingType;
    NSArray *cityName;
    NSArray *provienceName;
    NSArray *areaName;
    NSArray *garageType;
    NSString * textFieldName;
    UIPickerView *pickerView1;
    bool check;
}

@property (strong, nonatomic) IBOutlet UIButton *btn_featureCheckbox;
@property (strong, nonatomic) IBOutlet UIScrollView *post_sv;

- (IBAction)btn_saveData:(UIButton *)sender;

@property (strong, nonatomic) IBOutlet UIButton *addRoom_btn;

@property (weak, nonatomic) IBOutlet UIButton *rbtn_rent;
@property (weak, nonatomic) IBOutlet UIButton *rbtn_share;
@property (strong, nonatomic) IBOutlet UITextField *txt_post_Title;

@property (strong, nonatomic) IBOutlet UIButton *rbtn_apartment;
@property (strong, nonatomic) IBOutlet UIButton *rbtn_hostel;
@property (strong, nonatomic) IBOutlet UITextField *txt_city;
@property (strong, nonatomic) IBOutlet UITextField *txt_provience;
@property (strong, nonatomic) IBOutlet UITextField *txt_area;
@property(nonatomic,retain) IBOutlet UIPickerView *pickerView1;

@property (nonatomic, retain) NSString *sessid;
@property (nonatomic, retain) NSString *session_name;
@property (nonatomic, retain) NSString *token;
@property (strong, nonatomic) IBOutlet UITextField *txt_location;
@property (strong, nonatomic) IBOutlet UITextField *txt_street;
@property (strong, nonatomic) IBOutlet UITextField *txt_additional;
@property (strong, nonatomic) IBOutlet UITextField *txt_bedroom;

@property (strong, nonatomic) IBOutlet UITextField *txt_bathroom;
@property (strong, nonatomic) IBOutlet UITextField *txt_garage;


//- (IBAction)txtClickPV:(id)sender;

- (IBAction)propertystatus_action:(id)sender;

- (IBAction)propertytype_action:(id)sender;

- (IBAction)featureAction:(id)sender;
- (IBAction)btn_addRoom:(id)sender;

-(void)btnClick:(UIButton *)sender;
-(void)btnClickPT:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_parkingType;



@end
