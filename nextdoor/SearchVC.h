//
//  SearchVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 26/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVC : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btn_search;
- (IBAction)onClick_btn_search:(id)sender;

@end
