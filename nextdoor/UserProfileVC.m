//
//  UserProfileVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 22/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "UserProfileVC.h"
#import <QuartzCore/QuartzCore.h>
#import "LoginViewController.h"

@interface UserProfileVC ()

@end

@implementation UserProfileVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *userState =[prefs stringForKey:@"state"];
    NSLog(@"Login button value %@" , userState);
    
    
    if ([userState isEqualToString:@"visiter"]) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Access Denied"
                                                           message:@"Please Login first"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        
        [alertView show];
        
        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
    }else{

    _profileImage.layer.cornerRadius = _profileImage.frame.size.width / 3;
    _profileImage.clipsToBounds = YES;

    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    
    NSString *uid = [prefs stringForKey:@"uid"];
    NSString *mail = [prefs stringForKey:@"mail"];
    NSString *name = [prefs stringForKey:@"name"];
    NSString *mobile = [prefs stringForKey:@"mobile_no"];
    _lbl_mail.text = mail;
    _lbl_name.text = name;
    _lbl_mobile_no.text = mobile;
    NSLog(@"new string %@", uid);
    NSLog(@"new string %@", mail);
        
    }
  }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
