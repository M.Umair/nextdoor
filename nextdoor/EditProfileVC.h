//
//  EditProfileVC.h
//  nextdoor
//
//  Created by Muhammad Umair Ayoub on 18/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController<UITextFieldDelegate , UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scroll_View;
@property (strong, nonatomic) IBOutlet UILabel *lbl_userName;
@property (strong, nonatomic) IBOutlet UIButton *btn_uploadPhoto;
@property (strong, nonatomic) UIImagePickerController *picker;
- (IBAction)btn_ocClick_selectPhoto:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;
@property (strong, nonatomic) IBOutlet UITextField *txt_firstName;
@property (strong, nonatomic) IBOutlet UITextField *txt_lastName;
@property (strong, nonatomic) IBOutlet UITextField *txt_email;
@property (strong, nonatomic) IBOutlet UITextField *txt_mobileNumber;
@property (strong, nonatomic) IBOutlet UIImageView *img_profile;
- (IBAction)btn_onClick_uploadPhoto:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btn_save;

@property (strong, nonatomic) IBOutlet UITextField *txt_password;
@property (strong, nonatomic) IBOutlet UITextField *txt_confirmPassword;
- (IBAction)onClick_btn_save:(id)sender;

@end
