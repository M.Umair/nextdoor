//
//  UserProfileVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 22/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileVC : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *profileImage;


@property (strong, nonatomic) IBOutlet UILabel *lbl_mail;
@property (strong, nonatomic) IBOutlet UILabel *lbl_mobile_no;
@property (strong, nonatomic) IBOutlet UILabel *lbl_name;

@end
