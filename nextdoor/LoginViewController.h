//
//  LoginViewController.h
//  nextdoor
//
//  Created by Muhammad Usman on 15/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btn_login;
@property (strong, nonatomic) IBOutlet UIButton *btn_glogin;
@property (strong, nonatomic) IBOutlet UIButton *btn_fblogin;
@property (strong, nonatomic) IBOutlet UIButton *btn_caguest;

@property (strong, nonatomic) IBOutlet UITextField *txt_username;
@property (strong, nonatomic) IBOutlet UITextField *txt_password;

@property (nonatomic, retain) NSString *sessid;
@property (nonatomic, retain) NSString *session_name;
@property (nonatomic, retain) NSString *token;

- (IBAction)onClick_btn_Continue:(id)sender;

- (IBAction)onClick_btn_SignUP:(id)sender;
- (IBAction)onClick_btn_Login:(id)sender;
- (IBAction)onClick_btn_forgotPass:(id)sender;
@end
