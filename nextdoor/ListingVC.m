//
//  ListingVC.m
//  nextdoor
//
//  Created by Muhammad Usman on 20/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "ListingVC.h"
#import "ListingCell.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"
#import "HelloWorldViewController.h"
#import "PropertyDetailVC.h"


@interface ListingVC ()
{
    
    NSMutableArray *responseArray;
    
    NSDictionary *responseDict;
    NSData *responseDicti;
    
}

@end

@implementation ListingVC{
    NSArray *tableData;
    NSArray *pics;
    NSArray *time;
    NSArray *post_title;
    NSDictionary *post_status;
    NSArray *titls;
    NSMutableArray *str;
}

@synthesize tbl_View;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    

//    self.tableView.delegate = self;
    self.tbl_View.dataSource = self;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NSURL *url = [NSURL URLWithString:@"http://mantis.vu.edu.pk/nextdoor/postAdd/node.json?parameters[type]=property&parameters[status]=1"];
    
    
    
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *jsonOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:urlRequest success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
    
                                            {
                                                
                                                responseDict = (NSDictionary *)JSON;
                                                
                                                
                                                
                                                //NSLog(@"response is ################ %@",responseDict);
                                                
                                                
                                                
                                              
                                                
                                                responseArray =[[NSMutableArray alloc] init];
                                                
                                                
                                                for (NSDictionary *objects in responseDict)
                                                {
                                                    
                                                    
                                                    [responseArray addObject:objects];
                                                    
                                                }
                                                
                                                [tbl_View reloadData];
                                                
                                                
                                                
                                                
                            
                                            }
                                                                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON)
                                            {
                                                
                                                
                                                
                                                
                                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops something went wrong."
                                                                                                    message:[error localizedDescription]
                                                                                                   delegate:nil
                                                                                          cancelButtonTitle:@"Ok"
                                                                                          otherButtonTitles:nil];
                                                [alertView show];
                                                
                                                
                                            }];
    
    [jsonOperation start];

    
     self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    
    return responseArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    static NSString *cellidentifier = @"Cell";
    
    ListingCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath] ;
    
    NSDictionary *resultDict = [responseArray objectAtIndex:indexPath.row];
   
    cell.propertnamelbl.text = [resultDict valueForKey:@"title"];

    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *resultDict = [responseArray objectAtIndex:indexPath.row];

    
    PropertyDetailVC *propertyDetail = (PropertyDetailVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"PropertyDetailVC"];
    
    propertyDetail.nodeID = [resultDict valueForKey:@"nid"];
    
    NSString *nid = [resultDict valueForKey:@"nid"];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // saving an NSString
    
    [prefs setObject:nid forKey:@"uid"];

    [prefs synchronize];
    [self.navigationController pushViewController:propertyDetail animated:YES];
    
    
    
}



@end
