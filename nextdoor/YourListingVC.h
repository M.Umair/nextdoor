//
//  YourListingVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 21/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YourListingVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tbl_yourlistingview;

@property (strong, nonatomic) IBOutlet UITableView *tbl_View;


@end
