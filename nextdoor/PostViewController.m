//
//  PostViewController.m
//  nextdoor
//
//  Created by Muhammad Usman on 16/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import "PostViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "RoomDataVC.h"
#import "AFNetworking.h"
#import "AFHTTPRequestOperation.h"
#import "LoginViewController.h"
@interface PostViewController ()
{
    NSMutableArray *roomsArray;
}
@end

@implementation PostViewController{
    
}
@synthesize btn_featureCheckbox;
@synthesize rbtn_rent;
@synthesize rbtn_share;
@synthesize rbtn_apartment,rbtn_hostel;
@synthesize txt_city,txt_area,txt_provience,txt_parkingType, pickerView1;
@synthesize txt_post_Title;
@synthesize sessid = _sessid;
@synthesize session_name = _session_name;
@synthesize token = _token;


int tagval;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *userState =[prefs stringForKey:@"state"];
    NSLog(@"Login button value %@" , userState);
    
    
    if ([userState isEqualToString:@"visiter"]) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:@"Access Denied"
                                                           message:@"Please Login first"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        
        [alertView show];
        
        [self.parentViewController.navigationController popToRootViewControllerAnimated:YES];
    }else{
        
        
        roomsArray = [[NSMutableArray alloc] init];
        _post_sv.contentSize = CGSizeMake(self.view.frame.size.width, _addRoom_btn.frame.origin.y + _addRoom_btn.frame.size.height +20);
        //  _romm_sv.contentSize = CGSizeMake(self.view.frame.size.width, _btn_savePost.frame.origin.y+_btn_savePost.frame.size.height +270);
        
        [super viewDidLoad];
        
        
        
        
        
        
        // Do any additional setup after loading the view.
        //cornerRadius code
        //   _addRoom_btn.layer.cornerRadius=7;
        //checkbox
        check = NO;
        
        //radio button share and rent code
        [rbtn_rent setSelected:NO];
        [rbtn_share setSelected:NO];
        
        [rbtn_rent setImage:[UIImage imageNamed:@"radiobutton-choice-math-calculator-128.png"] forState:UIControlStateSelected];
        rbtn_rent.layer.cornerRadius=15;
        rbtn_rent.layer.masksToBounds=YES;
        [rbtn_rent addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [rbtn_share setImage:[UIImage imageNamed:@"radiobutton-choice-math-calculator-128.png"] forState:UIControlStateSelected];
        rbtn_share.layer.cornerRadius=15;
        rbtn_share.layer.masksToBounds=YES;
        [rbtn_share addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [rbtn_apartment setImage:[UIImage imageNamed:@"radiobutton-choice-math-calculator-128.png"] forState:UIControlStateSelected];
        rbtn_apartment.layer.cornerRadius=15;
        rbtn_apartment.layer.masksToBounds=YES;
        [rbtn_apartment addTarget:self action:@selector(btnClickPT:) forControlEvents:UIControlEventTouchUpInside];
        
        [rbtn_hostel setImage:[UIImage imageNamed:@"radiobutton-choice-math-calculator-128.png"] forState:UIControlStateSelected];
        rbtn_hostel.layer.cornerRadius=15;
        rbtn_hostel.layer.masksToBounds=YES;
        [rbtn_hostel addTarget:self action:@selector(btnClickPT:) forControlEvents:UIControlEventTouchUpInside];
        
        // picker view code
        
        [pickerView1 setHidden:YES];
        cityName = [[NSArray alloc]initWithObjects:@"",@"Lahore", nil];
        areaName = [[NSArray alloc]initWithObjects:@"Ichra",@"Model Town",@"Gulberg",@"Johor Town", nil];
        provienceName = [[NSArray alloc]initWithObjects:@"",@"Punjab", nil];
        garageType = [[NSArray alloc]initWithObjects:@"Direct Garage Access",@"Driveaway",@" Garage Door Opener", nil];
        
        /*
         =====================================
         navigation code
         =====================================*/
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    if(tagval==1){
        return [cityName count];
        
    }
    else if (tagval==2){
        
        return[provienceName count];
    }else if (tagval==3){
        
        return[areaName count];
    }else if (tagval==4){
        
        return[garageType count];
    }
    
    return 0;
}

- (NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if(tagval==1){
        return  [cityName objectAtIndex:row];
        
    }
    else if (tagval==2){
        
        return [provienceName objectAtIndex:row];
    }else if (tagval==3){
        
        return [areaName objectAtIndex:row];
    }else if (tagval==4){
        
        return[garageType objectAtIndex:row];
    }
    
    return 0;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(tagval==1){
        self.txt_city.text = [cityName objectAtIndex:row];
        pickerView.hidden=TRUE;
    }
    else if (tagval==2){
        self.txt_provience.text = [provienceName objectAtIndex:row];
        pickerView.hidden=TRUE;
    }
    else if (tagval==3){
        self.txt_area.text = [areaName objectAtIndex:row];
        pickerView.hidden=TRUE;
    }else if (tagval==4){
        
        self.txt_parkingType.text = [garageType objectAtIndex:row];
        pickerView.hidden=TRUE;
    }
    
}




//- (IBAction)txtClickPV:(id)sender {
//}

- (IBAction)propertystatus_action:(id)sender {
}

- (IBAction)propertytype_action:(id)sender {
}

- (IBAction)featureAction:(id)sender {
    
    if(!check){
        [btn_featureCheckbox setImage:[UIImage imageNamed:@"checked_checkbox.png"] forState: UIControlStateNormal];
        check = YES;
    }else if (check){
        
        [btn_featureCheckbox setImage:[UIImage imageNamed:@"unchecked_checkbox.png"] forState: UIControlStateNormal];
        check = NO;
    }
    
}


-(void)btnClick:(UIButton *)sender{
    int tagval1 = sender.tag;
    switch (tagval1) {
            
        case 0:
            if ([rbtn_rent isSelected]==YES) {
                [rbtn_rent setSelected:NO];
            }else {
                [rbtn_rent setSelected:YES];
                [rbtn_share setSelected:NO];
            }
            break;
        case 1:
            if ([rbtn_share isSelected]==YES) {
                [rbtn_share setSelected:NO];
            }else {
                [rbtn_share setSelected:YES];
                [rbtn_rent setSelected:NO];
            }
            break;
            
        default:
            break;
    }
}

-(void)btnClickPT:(UIButton *)sender{
    int tagval1 = sender.tag;
    switch (tagval1) {
            
        case 0:
            if ([rbtn_apartment isSelected]==YES) {
                [rbtn_apartment setSelected:NO];
            }else {
                [rbtn_apartment setSelected:YES];
                [rbtn_hostel setSelected:NO];
            }
            break;
        case 1:
            if ([rbtn_hostel isSelected]==YES) {
                [rbtn_hostel setSelected:NO];
            }else {
                [rbtn_hostel setSelected:YES];
                [rbtn_apartment setSelected:NO];
            }
            break;
            
        default:
            break;
    }
    
}

//-(void)txtPVaction:(UITextField *)sender{
//    tagval=sender.tag;
//
//}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [ pickerView1 setHidden:YES];
    if (txt_city.editing == YES)
    {
        tagval=1;
        [ pickerView1 setHidden:NO];
        [txt_city resignFirstResponder];
    }
    else
        if (txt_provience.editing == YES)
        {
            tagval =2;
            [ pickerView1 setHidden:NO];
            [txt_provience resignFirstResponder];
            
        }else if (txt_area.editing==YES){
            
            tagval =3;
            [ pickerView1 setHidden:NO];
            [txt_area resignFirstResponder];
            
        }else if (txt_parkingType.editing==YES){
            
            tagval =4;
            [ pickerView1 setHidden:NO];
            [txt_parkingType resignFirstResponder];
        }
    [pickerView1 reloadAllComponents];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void) fillData:(NSDictionary *)contactUs
{
    
    NSLog(@"Dictionary is %@",contactUs);
    
    [roomsArray addObject:contactUs];
    
    
    NSLog(@"rooms are %@",roomsArray);
    
}


- (IBAction)btn_addRoom:(id)sender {
    
    RoomDataVC *FVC = (RoomDataVC *)[self.storyboard instantiateViewControllerWithIdentifier:@"RoomDataVC"];
    
    FVC.FillItDelegate = self;
    
    [self.navigationController pushViewController:FVC animated:YES];
    
}

//------fields show up when keybord clicks---------
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
    
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self animateTextField:textField up:NO];
    
}

- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 100; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


//- (BOOL) textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

//---------end of fields showup code



#pragma mark -- NSURLCONNECTION DELEGATE

/*
 this method might be calling more than one times according to incoming data size
 */
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data


{
    
    NSError *error;
    
    NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    NSLog(@"response Dict %@", responseDict);
    
    
}
/*
 if there is an error occured, this method will be called by connection
 */
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"%@" , error);
}

/*
 if data is successfully received, this method will be called by connection
 */
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    //initialize convert the received data to string with UTF8 encoding
    //    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
    //                                              encoding:NSUTF8StringEncoding];
    //    NSLog(@"%@" , htmlSTR);
    //initialize a new webviewcontroller
    //    WebViewController *controller = [[WebViewController alloc] initWithString:htmlSTR];
    
    //show controller with navigation
    //    [self.navigationController pushViewController:controller animated:YES];
}


- (IBAction)btn_saveData:(UIButton *)sender {
    
    LoginViewController *loginviewcontroller = [[LoginViewController alloc]init];
    self.sessid = loginviewcontroller.sessid;
    NSLog(@"session id fron postttt %@" , self.sessid);
    
    NSMutableArray *property_status_und_value = [[NSMutableArray alloc] init];
    //NSDictionary *undval = [NSDictionary dictionaryWithObjectsAndKeys:@"13",@"value", nil];
    //[featuresArray addObject:undval];
    [property_status_und_value addObject:@"13"];
    
    // NSMutableArray *room_collection_und = [[NSMutableArray alloc] init];
    
    //    NSMutableArray *room_number = [[NSMutableArray alloc] init];
    //    NSDictionary *room_num = [NSDictionary dictionaryWithObjectsAndKeys:room_number,@"field_room_number",nil];
    //    NSDictionary *room_number_und = [NSDictionary dictionaryWithObjectsAndKeys:room_collection_und,@"und", nil];
    //    [room_number_und addObject:@"13"];
    
    //    NSDictionary *property_status = [NSDictionary dictionaryWithObjectsAndKeys:property_status_und_value,@"und", nil];
    //    NSDictionary *room_collection = [NSDictionary dictionaryWithObjectsAndKeys:room_collection_und,@"und", nil];
    //
    //    NSMutableArray *room_number_und = [[NSMutableArray alloc] init];
    //    NSDictionary *room_num_und_value = [NSDictionary dictionaryWithObjectsAndKeys:@"D12",@"value", nil];
    //    [room_number_und addObject:room_num_und_value];
    //    NSDictionary *room_num = [NSDictionary dictionaryWithObjectsAndKeys:room_number_und,@"und", nil];
    //    NSMutableArray *room_rent_und = [[NSMutableArray alloc] init];
    //    NSDictionary *room_rent_und_value = [NSDictionary dictionaryWithObjectsAndKeys:@"1200",@"value", nil];
    //    [room_rent_und addObject:room_rent_und_value];
    //
    //    NSDictionary *room_rent = [NSDictionary dictionaryWithObjectsAndKeys:room_rent_und,@"und", nil];
    //
    //    NSDictionary *room_number = [NSDictionary dictionaryWithObjectsAndKeys:room_num,@"field_room_number", room_rent , @"field_room_rent_" ,nil];
    //
    //    [room_collection_und addObject:room_number];
    //    NSDictionary *parameterDict = [NSDictionary dictionaryWithObjectsAndKeys:@"IOSTesting",@"title",
    //                                   @"property",@"type",
    //                                   property_status,@"field_property_status",
    //                                   room_collection , @"field_room_collection",
    //                                   nil];
    
    // Values Assign to Fields
    NSString *featured = @" ";
    if(check==YES){
        featured = @"1";
    }
    NSString *property_type=@"";
    if([rbtn_apartment isSelected]==YES){
        property_type = @"3";
    }else if([rbtn_hostel isSelected]==YES){
        property_type = @"22";
    }
    
    NSString *property_status=@"";
    if([rbtn_share isSelected]==YES){
        property_status = @"12";
    }else if([rbtn_rent isSelected]==YES){
        property_status = @"13";
    }
    
    NSString *city_and_area=@"23";
    if([txt_area.text isEqualToString:@"Ichra"]){
        city_and_area = @"24";
    }else if([txt_area.text isEqualToString:@"Johor Town"]){
        city_and_area = @"25";
    }else if([txt_area.text isEqualToString:@"Model Town"]){
        city_and_area = @"26";
    }else if([txt_area.text isEqualToString:@"Gulberg"]){
        city_and_area = @"27";
    }
    
    NSLog(@"City and Area from save button %@" , city_and_area);
    
    
    NSDictionary *paramDict =
    @{
      @"title" : txt_post_Title.text,
      @"type" : @"property",
      @"field_property_status":@{
              @"und":@[
                      property_status
                      ]
              },
      
      @"field_property_type":@{
              @"und":@[
                      property_type
                      ]
              },
      
      @"body":@{
              @"und":@[
                      @{
                          @"value":@"provide all facility."
                          
                          }
                      ]
              },
      
      
      
      @"field_beds":@{
              @"und":@[
                      _txt_bedroom.text
                      ]
              },
      
      @"field_bathrooms":@{
              @"und":@[
                      _txt_bathroom.text
                      ]
              },
      @"field_city_and_area" :     @{
              @"und" :         @[
                      @{
                          @"tid" : city_and_area
                          }
                      ]
              },
      @"field_featured": @{
              @"und": @[
                      featured
                      ]
              },
      @"field_garage": @{
              @"und": @[
                      @{
                          @"value": _txt_garage.text
                          }
                      ]
              },
      @"field_property_location": @{
              @"und": @[
                      @{
                          @"lid": @"49",
                          @"name": txt_area.text,
                          @"street": _txt_street.text,
                          @"additional": _txt_additional.text,
                          @"city": txt_city.text,
                          @"province": txt_provience.text,
                          @"postal_code": @"54000",
                          @"country": @"pk",
                          @"latitude": @"31.506090",
                          @"longitude": @"74.388593",
                          @"source": @"3",
                          @"is_primary": @"0",
                          @"province_name": @"Punjab",
                          @"country_name": @"Pakistan"
                          }
                      ]
              },
      @"field_appliances":@{
              @"und":@[
                      @"Dishwasher",
                      @"Freestanding Gas Range",
                      @"Garbage Disposal",
                      @"Microwave"
                      
                      ]
              },
      @"field_floors":@{
              @"und":@[
                      @{
                          @"value":@"2",
                          }
                      ]
              },
      @"field_parking_type":@{
              @"und":@[
                      txt_parkingType.text
                      
                      ]
              },
      
      @"field_features":@{
              @"und":@[
                      @"1",
                      @"2",
                      
                      @"4",
                      
                      @"6",
                      @"7",
                      @"8",
                      
                      ]
              },
      
      @"field_post_availability_":@{
              @"und":@[
                      @{
                          @"value":@"1"
                          }
                      ]
              },
      
      @"field_room_collection": @{
              @"und":@[
                      @{
                          @"field_room_number": @{
                                  @"und":@[
                                          @{
                                              @"value":@"12345"
                                              }
                                          ]
                                  },
                          
                          @"field_room_rent_": @{
                                  @"und":@[
                                          @{
                                              @"value":@"5000"
                                              }
                                          ]
                                  }
                          
                          }
                      ]
              
              }
      
      
      };
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    
    NSString *sessid = [prefs stringForKey:@"sessid"];
    NSString *session_name = [prefs stringForKey:@"session_name"];
    NSString *token = [prefs stringForKey:@"token"];
    
    NSString *session = [NSString stringWithFormat:@"%@"@"="@"%@" , session_name , sessid];
    
    // if(token.length ==0){
    
    
    NSMutableURLRequest *aRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://mantis.vu.edu.pk/nextdoor/postAdd/node.json"]];
    
    //    NSURLConnection *theConnection = [NSURLConnection connectionWithRequest:aRequest delegate:self];
    NSError *error;
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:paramDict
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    [aRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [aRequest addValue:token forHTTPHeaderField:@"X-CSRF-Token"];
    [aRequest addValue:session forHTTPHeaderField:@"session"];
    [aRequest setHTTPMethod:@"POST"];
    [aRequest setHTTPBody:dataFromDict];
    
    NSLog(@"http request is %@",aRequest);
    
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:aRequest delegate:self];
    
    NSLog(@"conn %@",conn);
    
    
    
    [conn start];
}
@end
