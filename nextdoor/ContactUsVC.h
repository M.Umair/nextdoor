//
//  ContactUsVC.h
//  nextdoor
//
//  Created by Muhammad Usman on 26/07/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol FillItDelegate <NSObject>

@optional

-(void) fillData:(NSDictionary *)contactUs;

@end


@interface ContactUsVC : UIViewController <UITextFieldDelegate,UITextViewDelegate,FillItDelegate>
@property (strong, nonatomic) IBOutlet UIButton *btn_sendMsg;
@property (strong, nonatomic) IBOutlet UIScrollView *contact_sv;
- (IBAction)onClick_send_Message:(id)sender;

@end
