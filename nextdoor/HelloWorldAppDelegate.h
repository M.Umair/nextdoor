//
//  HelloWorldAppDelegate.h
//  nextdoor
//
//  Created by faiza zahid on 30/06/2016.
//  Copyright (c) 2016 faiza zahid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelloWorldAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
